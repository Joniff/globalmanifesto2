﻿using System.Web.Mvc;
using Umbraco.Web.Models;

namespace GlobalManifesto.Website.Presentation.ErrorPage
{
    public class ErrorPageController : Management.Controllers.ControllerBase
    {
        public override ActionResult Index(RenderModel model)
        {
            var errorPage = Schema.ContentTypes.ErrorPage.Instance((Schema.DocTypes.ErrorPage) model.Content);
            Response.StatusCode = (int) errorPage.StatusCode;
            return CurrentTemplate<Schema.ContentTypes.ErrorPage>(errorPage);
        }
    }
}
