﻿using System.Web.Mvc;
using Umbraco.Web.Models;

namespace GlobalManifesto.Website.Presentation.SearchPage
{
    public class SearchPageController : Management.Controllers.ControllerBase
    {
        public override ActionResult Index(RenderModel model)
        {
            var viewModel = SearchPageViewModel.Instance((Schema.DocTypes.SearchPage) model.Content);
            viewModel.SearchText = (UmbracoRequest.UrlParamaters.Length > 0) ? UmbracoRequest.UrlParamaters[0] : null;

            return CurrentTemplate<SearchPageViewModel>(viewModel);
        }
    }
}
