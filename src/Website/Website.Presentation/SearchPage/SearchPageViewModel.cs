﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalManifesto.Website.Presentation.SearchPage
{
    public class SearchPageViewModel : Schema.ContentTypes.TextPage
    {
        public new static SearchPageViewModel Instance(Schema.DocTypes.SearchPage node)
        {
            return Schema.ContentTypes.Content<Schema.DocTypes.TextPage>.Instance<SearchPageViewModel>(node);
        }

        public string SearchText { get; internal set;}
    }
}
