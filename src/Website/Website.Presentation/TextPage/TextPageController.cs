﻿using System.Web.Mvc;
using Umbraco.Web.Models;


namespace GlobalManifesto.Website.Presentation.TextPage
{
    public class TextPageController : Management.Controllers.ControllerBase
    {
        public override ActionResult Index(RenderModel model)
        {
            var textPage = Schema.ContentTypes.TextPage.Instance((Schema.DocTypes.TextPage) model.Content);
            return CurrentTemplate<Schema.ContentTypes.TextPage>(textPage);
        }
    }
}
