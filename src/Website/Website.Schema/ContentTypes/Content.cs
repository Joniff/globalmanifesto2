﻿using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace GlobalManifesto.Website.Schema.ContentTypes
{
    public interface IContent : BaseTypes.IBase
    {
        Site Site { get; }
        Global Global { get; }
    }

    public abstract partial class Content<TDocType> : BaseTypes.Base<TDocType>, IContent where TDocType : PublishedContentModel
    {
        private Site site = null;
        public Site Site
        {
            get
            {
                if (site != null)
                {
                    return site;
                }
                //  Always return the site of the current page requested
                return site = Content<DocTypes.Site>.Instance<Site>(UmbracoContext.Current.PublishedContentRequest.PublishedContent.Ancestor(DocTypes.Site.ModelTypeAlias));
            }
        }

        private Global global = null;
        public Global Global
        {
            get
            {
                if (global != null)
                {
                    return global;
                }
                return global = Content<DocTypes.Global>.Instance<Global>(UmbracoContext.Current.ContentCache.GetSingleByXPath("//" + DocTypes.Global.ModelTypeAlias));
            }
        }
    }
}
