﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.ContentTypes
{
    public class ErrorPage : Content<DocTypes.ErrorPage>
    {
        public static ErrorPage Instance(DocTypes.ErrorPage node)
        {
            return Content<DocTypes.ErrorPage>.Instance<ErrorPage>(node);
        }

        public string Title
        {
            get
            {
                var vortoValue = new DataTypes.Vorto<string>(publishedBase.UmbracoTitle).Value;
                if (String.IsNullOrWhiteSpace(vortoValue))
                {
                    return Name;
                }
                return vortoValue;
            }
        }

        public IDictionary<ILanguage, string> TitleByLanguages
        {
            get
            {
                return new DataTypes.Vorto<string>(publishedBase.UmbracoTitle).Values;
            }
        }

        public IHtmlString Description
        {
            get
            {
                return new DataTypes.Vorto<IHtmlString>(publishedBase.BodyText).Value;
            }
        }

        public IDictionary<ILanguage, IHtmlString> DescriptionByLanguages
        {
            get
            {
                return new DataTypes.Vorto<IHtmlString>(publishedBase.BodyText).Values;
            }
        }

        public HttpStatusCode StatusCode
        {
            get
            {
                return new DataTypes.StatusCode(publishedBase.StatusCode ?? HttpStatusCode.NotFound).Value;
            }
        }

        private Compositions.Navigation navigation;
        public Compositions.Navigation Navigation
        {
            get
            {
                if (navigation != null)
                {
                    return navigation;
                }

                return navigation = Compositions.Navigation.Instance(new DocTypes.Navigation(publishedBase));
            }
        }

        public string Url
        {
            get
            {
                return publishedBase.Url;
            }
        }
    }
}
