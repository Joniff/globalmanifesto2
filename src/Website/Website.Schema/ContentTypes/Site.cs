﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Core.Configuration;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace GlobalManifesto.Website.Schema.ContentTypes
{
    public class Site : Content<DocTypes.Site>
    {
        public static Site Instance(DocTypes.Site node)
        {
            return Content<DocTypes.Site>.Instance<Site>(node);
        }

        public new static Site Instance(IPublishedContent node)
        {
            return Content<DocTypes.Site>.Instance<Site>(node);
        }

        public static Site Instance(int id)
        {
            return Content<DocTypes.Site>.Instance<Site>(PublishedItemType.Content, id);
        }

        private Page<DocTypes.Page> home = null;
        public Page<DocTypes.Page> Home
        {
            get
            {
                if (home != null)
                {
                    return home;
                }
                return home = publishedBase.UmbracoHome != null && publishedBase.UmbracoHome.Any() ? 
                    Page<DocTypes.Page>.Instance<Page<DocTypes.Page>>(new DocTypes.Page(publishedBase.UmbracoHome.First())) : null;
            }
        }

        private ErrorPage error = null;
        public ErrorPage Error
        {
            get
            {
                if (error != null)
                {
                    return error;
                }
                return error = publishedBase.UmbracoError != null && publishedBase.UmbracoError.Any() ? 
                    (ErrorPage) ErrorPage.Instance(new DocTypes.ErrorPage(publishedBase.UmbracoError.First())) : null;
            }
        }

        private ErrorPage serverError = null;
        public ErrorPage ServerError
        {
            get
            {
                if (serverError != null)
                {
                    return serverError;
                }
                return serverError = publishedBase.UmbracoServerError != null && publishedBase.UmbracoServerError.Any() ? 
                    (ErrorPage) ErrorPage.Instance(new DocTypes.ErrorPage(publishedBase.UmbracoServerError.First())) : null;
            }
        }

        public IHtmlString GoogleAnalyticsCode
        {
            get
            {
                return new HtmlString(publishedBase.GaCode);
            }
        }

		public string SitemapFrequency
		{
			get
            {
                return (string) publishedBase.SitemapFrequency;
            }
		}

        public decimal SitemapPriority
		{
			get
            {
                return new DataTypes.Decimal(publishedBase.SitemapPriority).Value;
            }
		}

        private string url;

        public string Url
        {
            get
            {
                if (url != null)
                {
                    return url;
                }
                var builder = new UriBuilder(Schema.UmbracoRequest.UmbracoRequestProvider.Current.Domain.DomainName);
                return url = (builder.Path.Length > 1 && UmbracoConfig.For.UmbracoSettings().RequestHandler.AddTrailingSlash && !builder.Path.EndsWith("/")) ? 
                    builder.Path + "/" : builder.Path;
            }
        }

        public string Title
        {
            get
            {
                if (publishedBase.Title == null)
                {
                    return publishedBase.Name;
                }
                var title = new DataTypes.Vorto<string>(publishedBase.Title).Value;
                return (string.IsNullOrWhiteSpace(title)) ? publishedBase.Name : title;
            }
        }

        public string RobotsTxt
        {
            get
            {
                return publishedBase.Robotstxt;
            }
        }

        [Serializable]
        public class SitemapXmlChild
        {
            public string Url { get; internal set; }
            public DateTime? LastModified { get; internal set; }

            public string Frequency { get; internal set; }

            public decimal? Priority { get; internal set; }
        }

        public IEnumerable<SitemapXmlChild> SitemapXml
        {
            get
            {
                var children = new List<SitemapXmlChild>();
                var descendants = UmbracoContext.Current.ContentCache.GetByXPath("//* [@isDoc and starts-with(@path, '" + publishedBase.Path + ",') and sitemapIndex = '1']");
                foreach (var descendant in descendants)
                {
                    var sitemap = ContentTypes.Compositions.Sitemap.Instance(new DocTypes.Sitemap(descendant));
                    var uri = new UriBuilder(new Uri(HttpContext.Current.Request.Url, descendant.Url));
                    uri.Scheme = HttpContext.Current.Request.Url.Scheme;
                    uri.Port = HttpContext.Current.Request.Url.Port;
                    uri.Host = HttpContext.Current.Request.Url.Host;

                    children.Add(new SitemapXmlChild()
                        {
                            Url = Uri.EscapeUriString(uri.Uri.AbsoluteUri),
                            LastModified = descendant.UpdateDate,
                            Frequency = String.IsNullOrWhiteSpace(sitemap.Frequency) ? SitemapFrequency : sitemap.Frequency,
                            Priority = sitemap.Priority
                        }
                    );
                }
                return children;
            }
        }

        private IEnumerable<ILanguage> languages;
        public IEnumerable<ILanguage> Languages
        {
            get
            {
                if (languages != null)
                {
                    return languages;
                }

                var results = new Dictionary<int, ILanguage>();

                foreach (var domain in Schema.Domains.DomainProvider.Current.Where(x => x.RootContentId == publishedBase.Id))
                {
                    if (domain.LanguageId != null && !results.ContainsKey((int) domain.LanguageId))
                    {
                        results.Add((int) domain.LanguageId, 
                            UmbracoContext.Current.Application.Services.LocalizationService.GetLanguageById((int) domain.LanguageId));
                    }
                }

                return languages = results.Values;
            }
        }
        private List<Tuple<IDomain, IEnumerable<ILanguage>>> domains;

        public IEnumerable<Tuple<IDomain, IEnumerable<ILanguage>>> Domains
        {
            get
            {
                if (domains != null)
                {
                    return domains;
                }

                domains = new List<Tuple<IDomain, IEnumerable<ILanguage>>>();

                foreach (var domain in Schema.Domains.DomainProvider.Current.Where(x => x.RootContentId == publishedBase.Id))
                {
                    IEnumerable<ILanguage> validLanguages = null;
                    if (domain.LanguageId != null)
                    {
                        if (publishedBase.Domains.PickedKeys.Contains(domain.DomainName))
                        {
                            validLanguages = new List<ILanguage>()
                            {
                                UmbracoContext.Current.Application.Services.LocalizationService.GetLanguageById((int) domain.LanguageId)
                            };
                        }
                        else
                        {
                            validLanguages = Languages.OrderBy(x => x.Id == domain.LanguageId ? 0 : 1);     //  Make primary language first
                        }
                    }
                    else
                    {
                        validLanguages = Languages;
                    }
                    domains.Add(new Tuple<IDomain, IEnumerable<ILanguage>>(domain, validLanguages));
                }

                return domains;
            }
        }
    }
}
