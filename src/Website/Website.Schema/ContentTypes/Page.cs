﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Core.Configuration;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace GlobalManifesto.Website.Schema.ContentTypes
{
    //  This exists for binding pages in the master template.
    public interface IPage : IContent
    {
        Compositions.Seo Seo { get; }
        Compositions.Navigation Navigation { get; }
        Compositions.Sitemap Sitemap { get; }

        string Url { get; }
    }

    public partial class Page<TDocType> : Content<TDocType>, IPage where TDocType : PublishedContentModel
    {
        public static Page<TDocType> Instance(TDocType node)
        {
            return Content<TDocType>.Instance<Page<TDocType>>(node);
        }

        private Compositions.Seo seo;
        public Compositions.Seo Seo
        {
            get
            {
                if (seo != null)
                {
                    return seo;
                }

                return seo = Compositions.Seo.Instance(new DocTypes.Seo(publishedBase));
            }
        }

        private Compositions.Navigation navigation;
        public Compositions.Navigation Navigation
        {
            get
            {
                if (navigation != null)
                {
                    return navigation;
                }

                return navigation = Compositions.Navigation.Instance(new DocTypes.Navigation(publishedBase));
            }
        }

        private Compositions.Sitemap sitemap;
        public Compositions.Sitemap Sitemap
        {
            get
            {
                if (sitemap != null)
                {
                    return sitemap;
                }

                return sitemap = Compositions.Sitemap.Instance(new DocTypes.Sitemap(publishedBase));
            }
        }

        public string Url
        {
            get
            {
                return publishedBase.Url;
            }
        }

        //  Only returns urls in all languages that are valid for the current domain
        public IDictionary<ILanguage, string[]> Urls
        {
            get
            {
                return Schema.Urls.CacheProvider.Current.Get(publishedBase.Id).
                    Where(x => x.Domain.Id == UmbracoContext.Current.PublishedContentRequest.UmbracoDomain.Id).ToDictionary(x => x.Language, y => y.Urls);
            }
        }

        public string UrlBuilder(params string[] wildcards)
        {
            var addTrailingSlash = UmbracoConfig.For.UmbracoSettings().RequestHandler.AddTrailingSlash;
            var results = new StringBuilder();
            var url = Url;
            if (url.EndsWith("/"))
            {
                url = url.Substring(0, url.Length - 1);
            }
            results.Append(url);

            foreach (var wildcard in wildcards)
            {
                results.Append("/");
                results.Append(wildcard);
            }
            if (addTrailingSlash)
            {
                results.Append("/");
            }
            return results.ToString();
        }

    }
}