﻿using System;
using System.Collections.Generic;
using System.Web;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.ContentTypes
{
    public class TextPage : Page<DocTypes.TextPage>
    {
        public new static TextPage Instance(DocTypes.TextPage node)
        {
            return Content<DocTypes.TextPage>.Instance<TextPage>(node);
        }

        public IHtmlString Description
        {
            get
            {
                return new DataTypes.Vorto<IHtmlString>(publishedBase.BodyText).Value;
            }
        }

        public IDictionary<ILanguage, IHtmlString> DescriptionByLanguages
        {
            get
            {
                return new DataTypes.Vorto<IHtmlString>(publishedBase.BodyText).Values;
            }
        }
    }
}
