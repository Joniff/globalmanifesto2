﻿using System;
using System.Collections.Generic;
using Umbraco.Web;

namespace GlobalManifesto.Website.Schema.ContentTypes
{
    public class Global : Content<DocTypes.Global>
    {
        public static Global Instance(DocTypes.Global node)
        {
            return Content<DocTypes.Global>.Instance<Global>(node);
        }
    }
}
