﻿namespace GlobalManifesto.Website.Schema.ContentTypes.Compositions
{
    public class Sitemap : Page<DocTypes.Sitemap>
    {
        public static new Sitemap Instance(DocTypes.Sitemap node)
        {
            return Content<DocTypes.Sitemap>.Instance<Sitemap>(node);
        }

		public string Frequency
        {
            get
            {
                return (string) publishedBase.SitemapFrequency;
            }
        }

        public bool Index
        {
            get
            {
                return publishedBase.SitemapIndex;
            }
        }

        public decimal Priority
        {
            get
            {
                return new DataTypes.Decimal(publishedBase.SitemapPriority).Value;
            }
        }
    }
}
