﻿using System.Collections.Generic;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.ContentTypes.Compositions
{
    public class Navigation : Page<DocTypes.Navigation>
    {
        public static new Navigation Instance(DocTypes.Navigation node)
        {
            return Content<DocTypes.Navigation>.Instance<Navigation>(node);
        }

        public IPage InternalRedirect
        {
            get
            {
                if (publishedBase.UmbracoInternalRedirectId == null || publishedBase.UmbracoInternalRedirectId.Id == Umbraco.Core.Constants.System.Root)
                {
                    return null;
                }
                object content = (object) Instance(publishedBase.UmbracoInternalRedirectId);
                if (content is IPage)
                {
                    return (IPage) content;
                }

                //  content isn't a page
                return null;
            }
        }

        public IPage Redirect
        {
            get
            {
                if (publishedBase.UmbracoRedirect == null || publishedBase.UmbracoRedirect.Id == Umbraco.Core.Constants.System.Root)
                {
                    return null;
                }
                object content = (object) Instance(publishedBase.UmbracoRedirect);
                if (content is IPage)
                {
                    return (IPage) content;
                }

                //  content isn't a page
                return null;
            }
        }

        public string[] ExtraUrls
        {
            get
            {
                return new DataTypes.Vorto<string[]>(publishedBase.UmbracoUrls).Value;
            }
        }

        public IDictionary<ILanguage, string[]> ExtraUrlsByLanguages
        {
            get
            {
                return new DataTypes.Vorto<string[]>(publishedBase.UmbracoUrls).Values;
            }
        }
    }
}
