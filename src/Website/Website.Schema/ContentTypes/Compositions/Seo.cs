﻿using System;
using System.Linq;
using System.Collections.Generic;
using Archetype.Models;
using Newtonsoft.Json;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.ContentTypes.Compositions
{
    public class Seo : Page<DocTypes.Seo>
    {
        bool hasInit = false;
        bool hasInits = false;

        private void init()
        {
            if (hasInit)
            {
                return;
            }
            hasInit = true;
            var languageValue = new DataTypes.Vorto<ArchetypeModel>(publishedBase.UmbracoSeo).Value;
            var fieldset = languageValue.Fieldsets.First();
            title = fieldset.GetValue<string>(DocTypes.Constants.SeoTitle);
            keywords = formatTags(fieldset.GetValue<string>(DocTypes.Constants.SeoKeywords));
            description = fieldset.GetValue<string>(DocTypes.Constants.SeoDescription);
        }

        private void inits()
        {
            if (hasInits)
            {
                return;
            }
            hasInits = true;
            titleByLanguages = new Dictionary<ILanguage, string>();
            keywordsByLanguages = new Dictionary<ILanguage, string>();
            descriptionByLanguages = new Dictionary<ILanguage, string>();

            foreach (var item in new DataTypes.Vorto<ArchetypeModel>(publishedBase.UmbracoSeo).Values)
            {
                var fieldset = item.Value.Fieldsets.First();
                titleByLanguages.Add(item.Key, fieldset.GetValue<string>(DocTypes.Constants.SeoTitle));
                keywordsByLanguages.Add(item.Key, formatTags(fieldset.GetValue<string>(DocTypes.Constants.SeoTitle)));
                descriptionByLanguages.Add(item.Key, fieldset.GetValue<string>(DocTypes.Constants.SeoDescription));
            }
        }


        private string formatTags(string tags)
        {
            tags = String.Join("", tags.Split('\n', '\r', '\"', '[', ']'));     //  Remove unwanted characters
            tags = String.Join(",", tags.Split(',').Select(x => x.Trim()));     //  Trim each value separately
            return tags;
        }

        public static new Seo Instance(DocTypes.Seo node)
        {
            return Content<DocTypes.Seo>.Instance<Seo>(node);
        }

        private string description;

        public string Description
        {
			get
            {
                init();
                return description;
            }
		}

        private Dictionary<ILanguage, string> descriptionByLanguages;

        public IDictionary<ILanguage, string> DescriptionByLanguages
        {
			get
            {
                inits();
                return descriptionByLanguages;
            }
		}


        private string title;
        public string Title
		{
			get
            {
                init();
                if (String.IsNullOrWhiteSpace(title))
                {
                    return Name;
                }
                return title;
            }
		}

        private Dictionary<ILanguage, string> titleByLanguages;

        public IDictionary<ILanguage, string> TitleByLanguages
        {
			get
            {
                inits();
                return titleByLanguages;
            }
		}

        private string keywords;
        public string Keywords
		{
            get
            {
                init();
                return keywords;
            }
		}

        private Dictionary<ILanguage, string> keywordsByLanguages;
        public Dictionary<ILanguage, string> KeywordsByLanguages
		{
            get
            {
                inits();
                return keywordsByLanguages;
            }
		}

    }
}
