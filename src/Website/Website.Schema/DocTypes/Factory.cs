﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.DocTypes
{
    /// <summary>
    /// Used to create DocType corresponding to the Alias of the IPublishedContent
    /// </summary>
    public class Factory
    {
        [DebuggerDisplay("{PublishedType} - {Alias} - {DocType.Name}")]
        internal class Cache
        {
            public PublishedItemType PublishedType;
            public string Alias;
            public Type DocType;
        }

        internal static string InstalledDocTypeKey(PublishedItemType publishedType, string alias)
        {
            return publishedType.ToString() + alias;
        }

        internal static IDictionary<string, Cache> InstalledDocTypes
        {
            get
            {
                const string cacheKey = "af40ce7e-9c2e-4faa-8e5d-bce9a8b1e69e";

                var types = HttpContext.Current.Application.Get(cacheKey) as IDictionary<string, Cache>;
                if (types != null)
                {
                    return types;
                }

                types = new Dictionary<string, Cache>();
                foreach (var docType in Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsClass && !x.IsAbstract && x.IsSubclassOf(typeof(DocTypes.DocType))))
                {
                    var publishedType = (PublishedItemType) docType.GetField("ModelItemType", 
                        BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).GetValue(null);
                    var alias = (string) docType.GetField("ModelTypeAlias", 
                        BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy).GetValue(null);

                    var cache = new Cache()
                    {
                        PublishedType = publishedType,
                        Alias = alias,
                        DocType = docType
                    };

                    types.Add(InstalledDocTypeKey(publishedType, alias), cache);
                }

                HttpContext.Current.Application.Set(cacheKey, types);
                return types;
            }
        }

        /// <summary>
        /// Create a DocType of the correct type, as defined by the node's alias
        /// </summary>
        /// <param name="node">Has to be a Content node</param>
        /// <returns></returns>
        public static Schema.DocTypes.DocType Instance(IPublishedContent node)
        {
            var key = InstalledDocTypeKey(node.ItemType, node.DocumentTypeAlias);
            var matching = InstalledDocTypes.Where(x => x.Key == key);
            if (matching.Any())
            {
                return (Schema.DocTypes.DocType) Activator.CreateInstance(matching.First().Value.DocType, node);
            }
            return null;
        }

    }
}
