//------------------------------------------------------------------------------
// <auto-generated>
//   This code was generated by a tool.
//
//    Umbraco.ModelsBuilder v3.0.2.93
//
//   Changes to this file will be lost if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;
using Umbraco.ModelsBuilder;
using Umbraco.ModelsBuilder.Umbraco;

namespace GlobalManifesto.Website.Schema.DocTypes
{
	/// <summary>Error Page</summary>
	[PublishedContentModel("errorPage")]
	public partial class ErrorPage : Page, INavigation
	{
#pragma warning disable 0109 // new is redundant
		public new const string ModelTypeAlias = "errorPage";
		public new const PublishedItemType ModelItemType = PublishedItemType.Content;
#pragma warning restore 0109

		public ErrorPage(IPublishedContent content)
			: base(content)
		{ }

#pragma warning disable 0109 // new is redundant
		public new static PublishedContentType GetModelContentType()
		{
			return PublishedContentType.Get(ModelItemType, ModelTypeAlias);
		}
#pragma warning restore 0109

		public static PublishedPropertyType GetModelPropertyType<TValue>(Expression<Func<ErrorPage, TValue>> selector)
		{
			return PublishedContentModelUtility.GetModelPropertyType(GetModelContentType(), selector);
		}

		///<summary>
		/// Description: Description of error
		///</summary>
		[ImplementPropertyType("bodyText")]
		public Our.Umbraco.Vorto.Models.VortoValue BodyText
		{
			get { return this.GetPropertyValue<Our.Umbraco.Vorto.Models.VortoValue>("bodyText"); }
		}

		///<summary>
		/// Status Code: Http Status Code
		///</summary>
		[ImplementPropertyType("statusCode")]
		public object StatusCode
		{
			get { return this.GetPropertyValue("statusCode"); }
		}

		///<summary>
		/// Title: Error page title
		///</summary>
		[ImplementPropertyType("umbracoTitle")]
		public Our.Umbraco.Vorto.Models.VortoValue UmbracoTitle
		{
			get { return this.GetPropertyValue<Our.Umbraco.Vorto.Models.VortoValue>("umbracoTitle"); }
		}

		///<summary>
		/// Internal Redirect: Any requests to this page, will instead return all the contents of this new page selected, without changing the Url shown
		///</summary>
		[ImplementPropertyType("umbracoInternalRedirectId")]
		public IPublishedContent UmbracoInternalRedirectId
		{
			get { return Navigation.GetUmbracoInternalRedirectId(this); }
		}

		///<summary>
		/// Redirect: Will redirect all requests to this selected page, changing the Url to this page too
		///</summary>
		[ImplementPropertyType("umbracoRedirect")]
		public IPublishedContent UmbracoRedirect
		{
			get { return Navigation.GetUmbracoRedirect(this); }
		}

		///<summary>
		/// Extra Urls: Add any extra Urls you would like for this page. Place each Url on its own line. All children content will also use these extra Urls as a base for their own Urls
		///</summary>
		[ImplementPropertyType("umbracoUrls")]
		public Our.Umbraco.Vorto.Models.VortoValue UmbracoUrls
		{
			get { return Navigation.GetUmbracoUrls(this); }
		}
	}
}
