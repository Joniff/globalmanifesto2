﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalManifesto.Website.Schema.DocTypes
{
    /*  
        
        If you are trying to regenerate your DocTypes, follow these 3 steps

          1. You will need to allow ModelBuilder access to your Umbraco website. This means it needs admin access to the Umbraco backend
             At the main menu select 'Tools' -> 'Option' -> and then scroll down to near the bottom and find 'Umbraco'
             Under ModelsBuilder options enter the credentials to your website. This includes the base URL used to view the website
                      
    
          2. Find this file in your Solution Explorer and right click, at the pop-up menu select 'Run Custom Tool'
             This will regenerate the Doctypes. If there are any errors, its is likely to do with access to the Umbraco site, repeat step 1

          3. You will now need to create new ContentTypes that consume these DocTypes

    */
}
