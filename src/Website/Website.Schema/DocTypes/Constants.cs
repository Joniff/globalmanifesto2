﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalManifesto.Website.Schema.DocTypes
{
    public class Constants
    {
        public const string UmbracoUrls = "umbracoUrls";
        public const string Title = "umbracoTitle";
        public const string Home = "umbracoHome";
        public const string Error = "umbracoError";
        public const string ServerError = "umbracoServerError";
        public const string Seo = "umbracoSeo";
        public const string SeoTitle = "title";
        public const string SeoKeywords = "keywords";
        public const string SeoDescription = "keywords";
		public readonly static string[] UrlParameters = {       //	Magic Label alias that contains one param
            "umbracoParam1",
            "umbracoParam2",
            "umbracoParam3",
            "umbracoParam4",
            "umbracoParam5"
        };
        public const string Wildcard = "*";
    }
}
