﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.Domains
{
    public class DomainProvider
    {
        const string cacheKey = "6f4e00bd-bd92-4c42-a328-11d3d5296ccd";

        public static IEnumerable<IDomain> Current
        {
            get
            {
                var request = HttpContext.Current.Application.Get(cacheKey) as IEnumerable<IDomain>;
                if (request != null)
                {
                    return request;
                }

                throw new NullReferenceException();
            }
            set
            {
                HttpContext.Current.Application.Set(cacheKey, value);
            }
        }
    }
}
