﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GlobalManifesto.Website.Schema.UmbracoRequest
{
    public class UmbracoRequestProvider
    {
        const string cacheKey = "cc7318fc-31fe-4f11-a541-667a29691c85";

        public static IUmbracoRequest Current
        {
            get
            {
                return HttpContext.Current.Items[cacheKey] as IUmbracoRequest;
            }
            set
            {
                HttpContext.Current.Items[cacheKey] = value;
            }
        }
    }
}
