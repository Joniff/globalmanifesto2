﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.UmbracoRequest
{
    public interface IUmbracoRequest
    {
        /// <summary>
        /// Domain of the current request
        /// </summary>
        IDomain Domain { get; }

        /// <summary>
        /// All the domains for this current site
        /// </summary>
        IEnumerable<IDomain> Domains { get; }

        /// <summary>
        /// Current Language, either hardcoded for this site or asked for by browser 
        /// </summary>
        ILanguage Language { get; }

        /// <summary>
        /// Fallback language, incase you don't wish or can't display current value
        /// </summary>
        ILanguage LanguageBackup { get; }

        /// <summary>
        /// Which site is this current request using
        /// </summary>
        Schema.ContentTypes.Site Site { get; }

        /// <summary>
        /// Any extra Url parameters that the request contained. Only valid if the content has a property with alias umbracoParam1
        /// </summary>
        string[] UrlParamaters { get; }
    }
}
