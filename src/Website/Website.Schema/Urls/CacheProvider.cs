﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace GlobalManifesto.Website.Schema.Urls
{
    public class CacheProvider
    {
        const string cacheKey = "98ef17ff-8087-4a9d-8fa5-d6f6ba7cccf9";

        public static ICache Current
        {
            get
            {
                var request = HttpContext.Current.Application.Get(cacheKey) as ICache;
                if (request != null)
                {
                    return request;
                }

                throw new NullReferenceException();
            }
            set
            {
                HttpContext.Current.Application.Set(cacheKey, value);
            }
        }
    }
}
