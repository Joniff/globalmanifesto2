﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Core.Cache;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.Urls
{
    public interface ICache
    {
        void Remove();
        void Remove(int id);
        void Add(int id, Multilingual urlDomain);
        Multilingual Get(int? id, IDomain domain, ILanguage language);
        IEnumerable<Multilingual> Get(int id);
        int? Get(IDomain domain, ILanguage language, string url);
        bool Match(IEnumerable<Multilingual> sources, IEnumerable<Multilingual> compares);
    }
}
