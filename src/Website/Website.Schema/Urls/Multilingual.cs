﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Schema.Urls
{
    public class Multilingual
    {
        public IDomain Domain;
        public ILanguage Language;
        public string[] Urls;
    }
}
