﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Core.Models.PublishedContent;
using Umbraco.Web;

namespace GlobalManifesto.Website.Schema.BaseTypes
{
    public interface IBase
    {
        int Id { get; }
        string Name { get; }
        PublishedItemType ItemType { get; }
    }

    public abstract partial class Base<TDocType> : IBase where TDocType : PublishedContentModel
    {
        protected TDocType publishedBase { get; set; }

        private static string CacheKey(PublishedItemType item, int id, Type type)
        {
            return "607b6e47-b369-44f1-98f2-33cbb8e9a4c7:" + item.ToString() + ":" + id.ToString() + ":" + type.Name;
        }

        public static TBaseType Instance<TBaseType>(TDocType node) where TBaseType : Base<TDocType>, new()
        {
            int id;
            PublishedItemType type;
            try
            {
                id = node.Id;
                type = node.ItemType;
            }
            catch (NullReferenceException)
            {
                //  Not a real node
                id = Umbraco.Core.Constants.System.Root;
                type = (PublishedItemType) typeof(TDocType).GetField("ModelItemType", BindingFlags.Public | BindingFlags.Static).GetValue(null);
            }

            string cacheKey = CacheKey(type, id, typeof(TBaseType));
            if (System.Web.HttpContext.Current.Items[cacheKey] is TBaseType)
            {
                return (TBaseType) System.Web.HttpContext.Current.Items[cacheKey];
            }
            //var content = new TBaseType();
            var content = (TBaseType) System.Runtime.Serialization.FormatterServices.GetUninitializedObject(typeof(TBaseType));
            content.publishedBase = (id == Umbraco.Core.Constants.System.Root) ? null : node;
            System.Web.HttpContext.Current.Items[cacheKey] = content;
            return content;
        }

        public static TBaseType Instance<TBaseType>(IPublishedContent node) where TBaseType : Base<TDocType>, new()
        {
            string cacheKey = CacheKey(node.ItemType, node.Id, typeof(TBaseType));
            if (System.Web.HttpContext.Current.Items[cacheKey] is TBaseType)
            {
                return (TBaseType) System.Web.HttpContext.Current.Items[cacheKey];
            }
            //var content = new TBaseType();
            var content = new TBaseType();
            try
            {
                content.publishedBase = (TDocType)Activator.CreateInstance(typeof(TDocType), node);
            }
            catch (MissingMethodException)
            {
                //  Was no public constructor, so now trying a non-public one
                content.publishedBase = (TDocType)typeof(TDocType)
                    .GetConstructor(
                      BindingFlags.NonPublic | BindingFlags.CreateInstance | BindingFlags.Instance, 
                      null, 
                      new[] { typeof(IPublishedContent) }, 
                      null
                    )
                    .Invoke(new object[] { node });
            }
            System.Web.HttpContext.Current.Items[cacheKey] = content;
            return content;
        }

        private static IPublishedContent GetContent(PublishedItemType item, int id)
        {
            switch (item)
            {
                case PublishedItemType.Content:
                    return UmbracoContext.Current.ContentCache.GetById(id);

                case PublishedItemType.Media:
                    return UmbracoContext.Current.MediaCache.GetById(id);

                case PublishedItemType.Member:
                    var memberShipHelper = new Umbraco.Web.Security.MembershipHelper(Umbraco.Web.UmbracoContext.Current);
                    return memberShipHelper.GetById(id);

            }
            throw new ArgumentException();
        }

        public static TBaseType Instance<TBaseType>(PublishedItemType item, int id) where TBaseType : Base<TDocType>, new()
        {
            string cacheKey = CacheKey(item, id, typeof(TBaseType));
            if (System.Web.HttpContext.Current.Items[cacheKey] is TBaseType)
            {
                return (TBaseType) System.Web.HttpContext.Current.Items[cacheKey];
            }
            return Instance<TBaseType>(GetContent(item, id));
        }

        public string Name
        {
            get
            {
                return publishedBase.Name;
            }
        }

        public int Id
        {
            get
            {
                return publishedBase.Id;
            }
        }

        public PublishedItemType ItemType
        {
            get
            {
                return publishedBase.ItemType;
            }
        }

        internal static bool IsDerivativeOf(Type generic, Type compare)
        {
            while (generic != null && generic != typeof(object))
            {
                if (generic == compare)
                {
                    return true;
                }
                generic = generic.BaseType;
            }
            return false;
        }

        private class CacheInstalledBaseTypes
        {
            public IDictionary<string, Tuple<Type, Type>> AliasToType;

            public IDictionary<Guid, DocTypes.Factory.Cache> BaseToAlias;
        }

        private static CacheInstalledBaseTypes InstalledContentTypes
        {
            get
            {
                const string cacheKey = "108f249e-b69e-4999-b09f-bdc5d3e41eed";

                var types = HttpContext.Current.Application.Get(cacheKey) as CacheInstalledBaseTypes;
                if (types != null)
                {
                    return types;
                }

                types = new CacheInstalledBaseTypes()
                {
                    AliasToType = new Dictionary<string, Tuple<Type, Type>>(),
                    BaseToAlias = new Dictionary<Guid, DocTypes.Factory.Cache>()
                };

                foreach (var docType in DocTypes.Factory.InstalledDocTypes)
                {
                    var genericType = typeof(Base<>).MakeGenericType(new Type[] { docType.Value.DocType });

                    var matches = Assembly.GetExecutingAssembly().GetTypes().Where(x => x.IsClass && !x.IsAbstract && IsDerivativeOf(x, genericType));
                    if (!matches.Any())
                    {
                        continue;
                    }
                    var contentType = matches.First();
                    types.AliasToType.Add(docType.Key, new Tuple<Type, Type>(contentType, docType.Value.DocType));
                    types.BaseToAlias.Add(contentType.GUID, docType.Value);
                }

                HttpContext.Current.Application.Set(cacheKey, types);
                return types;
            }
        }

        private static IBase Instance(IPublishedContent node, Type baseType, Type docType)
        {
            var method = baseType.GetMethod("Instance", BindingFlags.Public | BindingFlags.Static);
            if (method.IsGenericMethod)
            {
                //  We think it returns ContentType<DocType>
                method = method.MakeGenericMethod(new[] { docType });
            }
            return (IBase) method.Invoke(null, new object[] { (Schema.DocTypes.DocType) Activator.CreateInstance(docType, node) });
        }

        /// <summary>
        /// Tries to return closest ContentType as specified in the alias value in node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public static IBase Instance(IPublishedContent node)
        {
            var key = DocTypes.Factory.InstalledDocTypeKey(node.ItemType, node.DocumentTypeAlias);
            var matching = InstalledContentTypes.AliasToType.Where(x => x.Key == key);
            if (matching.Any())
            {
                var contentType = matching.First();
                return Instance(node, contentType.Value.Item1, contentType.Value.Item2);
            }
            return null;
        }

        /// <summary>
        /// Tries to return closest ContentType as specified in the alias value in node
        /// </summary>
        /// <param name="id">Id of the node</param>
        /// <returns></returns>
        public static IBase Instance(PublishedItemType item, int id)
        {
            return Instance(GetContent(item, id));
        }

        /// <summary>
        /// Get all the children that can fit in the type TBaseType
        /// </summary>
        /// <typeparam name="TBaseType"></typeparam>
        /// <param name="includeDerivativeContent">When true, will find any content that is of type TBaseType or inherits TBaseType. This search will always be slower than false which only find content of type TBaseType</param>
        /// <returns></returns>
        public IEnumerable<TBaseType> Children<TBaseType>(bool includeDerivativeContent = false) where TBaseType : IBase
        {
            if (includeDerivativeContent)
            {
                foreach (var child in publishedBase.Children)
                {
                    var key = DocTypes.Factory.InstalledDocTypeKey( child.ItemType, child.DocumentTypeAlias);
                    var matching = InstalledContentTypes.AliasToType.Where(x => x.Key == key && IsDerivativeOf(x.Value.Item1, typeof(TBaseType)));
                    if (matching.Any())
                    {
                        yield return (TBaseType) Instance(child, typeof(TBaseType), matching.First().Value.Item2);
                    }
                }
            }
            else
            {
                DocTypes.Factory.Cache cache = null;
                if (!InstalledContentTypes.BaseToAlias.TryGetValue(typeof(TBaseType).GUID, out cache))
                {
                    //  This ContentType doesn't match any existing DocTypes
                    yield break;
                }

                var method = typeof(TBaseType).GetMethod("Instance", BindingFlags.Public | BindingFlags.Static);
                if (method.IsGenericMethod)
                {
                    //  We think it returns ContentType<DocType>
                    method = method.MakeGenericMethod(new[] { cache.DocType });
                }

                foreach (var child in publishedBase.Children(x => String.Compare(x.DocumentTypeAlias, cache.Alias, true) == 0))
                {
                    yield return (TBaseType) method.Invoke(null, new object[] { Activator.CreateInstance(cache.DocType, child) });
                }
            }
        }

        /// <summary>
        /// Get all the children
        /// </summary>
        /// <typeparam name="TBaseType"></typeparam>
        /// <returns></returns>
        public IEnumerable<IBase> Children()
        {
            foreach (var child in publishedBase.Children)
            {
                yield return Instance(child);
            }
        }

        /// <summary>
        /// Get all the children that can fit in the type TBaseType
        /// </summary>
        /// <typeparam name="TBaseType"></typeparam>
        /// <param name="includeDerivativeContent">When true, will find any content that is of type TBaseType or inherits TBaseType. This search will always be slower than false which only find content of type TBaseType</param>
        /// <returns></returns>
        public IEnumerable<TBaseType> Descendants<TBaseType>(bool includeDerivativeContent = false, string query = null) where TBaseType : IBase
        {
            var xpath = new StringBuilder();
            xpath.Append("//");
            string alias;
            MethodInfo method = null;
            DocTypes.Factory.Cache cache = null;
            if (includeDerivativeContent)
            {
                alias = "*";
            }
            else
            {
                if (!InstalledContentTypes.BaseToAlias.TryGetValue(typeof(TBaseType).GUID, out cache))
                {
                    //  This ContentType doesn't match any existing DocTypes
                    yield break;
                }

                method = typeof(TBaseType).GetMethod("Instance", BindingFlags.Public | BindingFlags.Static);
                if (method.IsGenericMethod)
                {
                    //  We think it returns ContentType<DocType>
                    method = method.MakeGenericMethod(new[] { cache.DocType });
                }
                alias = cache.Alias;
            }

            xpath.Append(alias);
            xpath.Append(" [(@isDoc and starts-with(@path, '");
            xpath.Append(publishedBase.Path);
            xpath.Append(",'))");
            if (!String.IsNullOrWhiteSpace(query))
            {
                xpath.Append(" and (");
                xpath.Append(query);
                xpath.Append(")");
            }
            xpath.Append("]");

            var children = (publishedBase.ItemType == PublishedItemType.Content) ? 
                UmbracoContext.Current.ContentCache.GetByXPath(xpath.ToString()) :
                UmbracoContext.Current.MediaCache.GetByXPath(xpath.ToString());

            foreach (var child in children)
            {
                if (includeDerivativeContent)
                {
                    var key = DocTypes.Factory.InstalledDocTypeKey(child.ItemType, child.DocumentTypeAlias);
                    var matching = InstalledContentTypes.AliasToType.Where(x => x.Key == key && IsDerivativeOf(x.Value.Item1, typeof(TBaseType)));
                    if (!matching.Any())
                    {
                        yield return (TBaseType) Instance(child, typeof(TBaseType), matching.First().Value.Item2);
                    }
                }
                else
                {
                    yield return (TBaseType) method.Invoke(null, new object[] { Activator.CreateInstance(cache.DocType, child) });
                }
            }
        }

        /// <summary>
        /// Get all the descendants
        /// </summary>
        /// <typeparam name="TBaseType"></typeparam>
        /// <returns></returns>
        public IEnumerable<IBase> Descendants()
        {
            foreach (var child in publishedBase.Descendants())
            {
                yield return Instance(child);
            }
        }

        public IBase Parent()
        {
            if (publishedBase.Id == Umbraco.Core.Constants.System.Root || publishedBase.Parent.Id == Umbraco.Core.Constants.System.Root)
            {
                return null;
            }

            return Instance(publishedBase.Parent);
        }

        public TBaseType Parent<TBaseType>() where TBaseType : IBase
        {
            if (publishedBase.Id == Umbraco.Core.Constants.System.Root || publishedBase.Parent.Id == Umbraco.Core.Constants.System.Root)
            {
                return default(TBaseType);
            }

            string alias;
            MethodInfo method = null;
            DocTypes.Factory.Cache cache = null;
            if (!InstalledContentTypes.BaseToAlias.TryGetValue(typeof(TBaseType).GUID, out cache))
            {
                //  This ContentType doesn't match any existing DocTypes
                return default(TBaseType);
            }

            method = typeof(TBaseType).GetMethod("Instance", BindingFlags.Public | BindingFlags.Static);
            if (method.IsGenericMethod)
            {
                //  We think it returns ContentType<DocType>
                method = method.MakeGenericMethod(new[] { cache.DocType });
            }
            alias = cache.Alias;
            return (TBaseType) method.Invoke(null, new object[] { Activator.CreateInstance(cache.DocType, publishedBase.Parent) });
        }


    }
}
