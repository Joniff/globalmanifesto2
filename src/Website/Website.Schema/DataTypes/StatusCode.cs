﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GlobalManifesto.Website.Schema.DataTypes
{
    public class StatusCode
    {
        HttpStatusCode number;

        public StatusCode(object value)
        {
            if (value is HttpStatusCode)
            {
                number = (HttpStatusCode) value;
            }
            else if (value is decimal)
            {
                number = (HttpStatusCode) value;
            }
            else if (value is float)
            {
                number = (HttpStatusCode) ((float) value);
            }
            else if (value is double)
            {
                number = (HttpStatusCode) ((double) value);
            }
            else if (value is int)
            {
                number = (HttpStatusCode) ((int) value);
            }
            else if (value is string)
            {
                int code = 0;
                if (int.TryParse((string) value, out code))
                {
                    number = (HttpStatusCode) code;
                }
            }
            else
            {
                number = (HttpStatusCode) Convert.ChangeType(value, typeof(HttpStatusCode), CultureInfo.InvariantCulture);
            }
        }

        public HttpStatusCode Value
        {
            get
            {
                return number;
            }
        }

    }
}
