﻿using Archetype.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace GlobalManifesto.Website.Schema.DataTypes
{
    public class Vorto<T> 
    {
        public class LanguageCompare : IEqualityComparer<ILanguage>
        {
            public bool Equals(ILanguage x, ILanguage y)
            {
                return x.Id == y.Id;
            }
            public int GetHashCode(ILanguage x)
            {
                return x.Id;
            }
        }

        private char arraySeperator = '\n';
        private Our.Umbraco.Vorto.Models.VortoValue vorto;
        private IDictionary<ILanguage, T> values = null;

        public Vorto(char seperator = '\n')
        {
            vorto = (Our.Umbraco.Vorto.Models.VortoValue)System.Runtime.Serialization.FormatterServices.GetUninitializedObject(typeof(Our.Umbraco.Vorto.Models.VortoValue));
            vorto.DtdGuid = Guid.NewGuid();
            arraySeperator = seperator;
            Init();
        }

        public Vorto(Our.Umbraco.Vorto.Models.VortoValue value, char seperator = '\n')
        {
            vorto = value;
            arraySeperator = seperator;
            Init();
        }

        public Vorto(string value, char seperator = '\n')
        {
            vorto = JsonConvert.DeserializeObject<Our.Umbraco.Vorto.Models.VortoValue>(value);
            arraySeperator = seperator;
            Init();
        }

        private void Init()
        {
            values = new Dictionary<ILanguage, T>(new LanguageCompare());
            if (vorto == null || vorto.Values == null)
            {
                return;
            }

            foreach (var entry in vorto.Values)
            {
                T newValue = SetValue(entry.Value);
                values.Add(UmbracoContext.Current.Application.Services.LocalizationService.GetLanguageByIsoCode(entry.Key), newValue);
            }
        }

        private object GetValue(T value)
        {
            if (typeof(T).IsArray && typeof(T).GetElementType() == typeof(string))
            {
                //  Want a string[] but given a string
                return (object) String.Join(arraySeperator.ToString(), (string[]) (object) value);
            }
            if (typeof(T).IsEquivalentTo(typeof(IHtmlString)))
            {
                return ((IHtmlString) value).ToString();
            }
            if (typeof(T).IsEquivalentTo(typeof(ArchetypeModel)))
            {
                return (object) JsonConvert.SerializeObject(value);
            }
            return value;
        }

        private T SetValue(object value)
        {
            if (value is T)
            {
                return (T) value;
            }
            if (value is string && typeof(T).IsArray)
            {
                return (T) (object) ((string)value).Split(arraySeperator);
            }
            if (typeof(T).IsEquivalentTo(typeof(IHtmlString)))
            {
                return (T) (object) new HtmlString((string) (object) value);
            }
            if (typeof(T).IsEquivalentTo(typeof(ArchetypeModel)))
            {
                return (T) (object) JsonConvert.DeserializeObject<ArchetypeModel>((string) (object)value);
            }
            return (T) Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }

        public T Value
        {
            get
            {
                if (!values.Any())
                {
                    return default(T);
                }

                var found = values.Where(x => String.Compare(x.Key.IsoCode, System.Threading.Thread.CurrentThread.CurrentUICulture.Name, true) == 0);
                if (found.Any() && found.First().Value != null)
                {
                    return found.First().Value;
                }
                found = values.Where(x => String.Compare(x.Key.IsoCode, UmbracoContext.Current.PublishedContentRequest.UmbracoDomain.LanguageIsoCode, true) == 0);
                if (found.Any() && found.First().Value != null)
                {
                    return found.First().Value;
                }
                return values.First().Value;
            }

            set
            {
                var language = UmbracoContext.Current.Application.Services.LocalizationService.GetLanguageByIsoCode(System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
                if (values.ContainsKey(language))
                {
                    values.Remove(language);
                }
                values.Add(language, value);
            }
        }

        public IDictionary<ILanguage, T> Values
        {
            get
            {
                return values;
            }

            set
            {
                values = value;
            }
        }

        public string Property
        {
            get
            {
                vorto.Values = values.ToDictionary(x => x.Key.IsoCode, y => GetValue(y.Value));
                return JsonConvert.SerializeObject(vorto);
            }
        }
    }
}
