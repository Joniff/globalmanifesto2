﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalManifesto.Website.Schema.DataTypes
{
    public class Decimal
    {
        decimal number;

        public Decimal(object value)
        {
            if (value is decimal)
            {
                number = (decimal) value;
            }
            else if (value is float)
            {
                number = (decimal) ((float) value);
            }
            else if (value is double)
            {
                number = (decimal) ((double) value);
            }
            else if (value is int)
            {
                number = (decimal) ((int) value);
            }
            else
            {
                number = (decimal) Convert.ChangeType(value, typeof(decimal), CultureInfo.InvariantCulture);
            }
        }

        public decimal Value
        {
            get
            {
                return number;
            }
        }
    }
}
