﻿using System;
using System.Collections.Generic;
using System.Linq;
//using Slimsy;
using Umbraco.Core.Models;
using Umbraco.Web;
using System.Configuration;

namespace GlobalManifesto.Website.Schema.MediaTypes
{
    public partial class Image : BaseTypes.Base<DocTypes.Image>
    {
        static readonly string MissingImage = ConfigurationManager.AppSettings["Umbraco.Media.ImageMissing"];

        public static Image Instance(DocTypes.Image node)
        {
            return BaseTypes.Base<DocTypes.Image>.Instance<Image>(node);
        }

        public string Url
        {
            get
            {
                return (publishedBase == null) ? MissingImage : publishedBase.Url;
            }
        }

        //public string ResponsiveUrl(int? width = null, int? height = null)
        //{
        //    return (publishedBase == null) ? MissingImage : publishedBase.GetResponsiveImageUrl(
        //        width == null ? int.Parse(publishedBase.UmbracoWidth) : (int) width, 
        //        height == null ? int.Parse(publishedBase.UmbracoHeight) : (int) height);
        //}

        //public string ResponsiveUrl(string cropAlias)
        //{
        //    return (publishedBase == null) ? MissingImage : publishedBase.GetResponsiveCropUrl(cropAlias);
        //}
    }
}