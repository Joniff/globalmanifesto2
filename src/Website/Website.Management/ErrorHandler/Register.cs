﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Web.Routing;

namespace GlobalManifesto.Website.Management.ErrorHandler
{
    public class Register : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Core.Logging.LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "#globalmanifesto-register1: ApplicationStarting");

            ContentFinderResolver.Current.AddType<ErrorContentFinder>();
        }
    }
}
