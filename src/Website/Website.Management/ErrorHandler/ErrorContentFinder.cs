﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace GlobalManifesto.Website.Management.ErrorHandler
{
    class ErrorContentFinder : IContentFinder
    {
        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            var request = Schema.UmbracoRequest.UmbracoRequestProvider.Current = new Multilingual.UmbracoRequest(contentRequest);
            if (request.Site == null || request.Site.Error == null)
            {
                return false;
            }
            contentRequest.PublishedContent = UmbracoContext.Current.ContentCache.GetById(request.Site.Error.Id);
            if (contentRequest.PublishedContent == null)
            {
                return false;
            }
            return true;
        }
    }
}
