﻿using System;
using System.Linq;
using System.Text;
using System.Web;
using Umbraco.Core.Configuration;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace GlobalManifesto.Website.Management.Multilingual
{
    public class MultilingualContentFinder : IContentFinder
    {
        public MultilingualContentFinder()
        {
            //addTrailingSlash = UmbracoConfig.For.UmbracoSettings().RequestHandler.AddTrailingSlash;
        }

        public bool TryFindContent(PublishedContentRequest contentRequest)
        {
            bool addTrailingSlash = false;
            if (contentRequest == null)
            {
                return false;
            }

            var finder = new UmbracoRequest(contentRequest);
            Schema.UmbracoRequest.UmbracoRequestProvider.Current = finder;
            if (finder.Site == null)
            {
                return false;
            }

            var builder = new UriBuilder(finder.Domain.DomainName);
            var originalUrl = contentRequest.Uri.AbsolutePath;
            if (originalUrl.Length < builder.Path.Length)
            {
                //  Ouch, seem to have the wrong domain
                return false;
            }
            var sourceUrl =  (builder.Path.Length == 1) ? originalUrl : (originalUrl.Length == builder.Path.Length) ? "/" : originalUrl.Substring(builder.Path.Length);
            var parts = sourceUrl.Split(new char[] {'/'});
            if (String.IsNullOrWhiteSpace(parts[parts.Length - 1]))
            {
                addTrailingSlash = true;
                Array.Resize<string>(ref parts, parts.Length - 1);
            }
            int maxLevel = (parts.Length > Schema.DocTypes.Constants.UrlParameters.Length) ? Schema.DocTypes.Constants.UrlParameters.Length : parts.Length;

            for (var level = 0; level != maxLevel; level++)
            {
                var pos = parts.Length - level;
                if (pos <= 0)
                {
                    return false;   //  Failed to find a Url
                }

                var url = new StringBuilder();
                
                for (var part = 0; part != parts.Length; part++)
                {
                    url.Append(part < pos ? parts[part] : Schema.DocTypes.Constants.Wildcard);
                    if (addTrailingSlash || part < parts.Length - 1)
                    {
                        url.Append('/');
                    }
                }
                if (url.Length == 1 && builder.Path.Length != 1)
                {
                    url.Clear();
                }
                var id = Schema.Urls.CacheProvider.Current.Get(finder.Domain, finder.Language, url.ToString());
                if (id != null)
                {
                    contentRequest.PublishedContent = UmbracoContext.Current.ContentCache.GetById((int) id);
                    if (contentRequest.PublishedContent != null)
                    {
                        var parameters = new string[level];
                        for (var index = 0; index != level; index++, pos++)
                        {
                            parameters[index] = HttpUtility.UrlDecode(parts[pos]);
                        }
                        finder.UrlParamaters = parameters;
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
