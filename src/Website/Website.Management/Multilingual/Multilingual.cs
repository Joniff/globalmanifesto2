﻿using Archetype.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core;
using Umbraco.Core.Configuration;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace GlobalManifesto.Website.Management.Multilingual
{
    public class Multilingual
    {
        public Multilingual()
        {
        }

        internal void RemoveAll()
        {
            Schema.Urls.CacheProvider.Current.Remove();
        }

        internal void Remove(int id)
        {
            Schema.Urls.CacheProvider.Current.Remove(id);
        }

        internal void Add(Schema.ContentTypes.Site site, AnyContent content, bool addTrailingSlash)
        {
            var urlDomains = new List<Schema.Urls.Multilingual>();
            var parentUrlDomains = (site.Id != content.ParentId) ? Schema.Urls.CacheProvider.Current.Get(content.ParentId) : Enumerable.Empty<Schema.Urls.Multilingual>();

            var userUrlsByLanguage = content.Urls;
            var titleByLanguage = content.Titles;
            bool isHomePage = site.Home != null && site.Home.Id == content.Id;

            foreach (var domain in site.Domains)
            {
                var builder = new UriBuilder(domain.Item1.DomainName);
                foreach (var language in domain.Item2)
                {
                    var urls = new HashSet<string>();

                    if (isHomePage)
                    {
                        urls.Add(builder.Path.Length == 1 ? "/" : "");
                    }

                    //  Add urls from parent + title
                    string title = null;
                    titleByLanguage.TryGetValue(language, out title);
                    if (title == null)
                    {
                        title = content.Name;
                    }
                    title = "/" + title.ToUrlSegment();
                    if (addTrailingSlash)
                    {
                        title = title + "/";
                    }

                    var parentUrlDomain = parentUrlDomains.Where(x => x.Domain.Id == domain.Item1.Id && x.Language.Id == language.Id);
                    if (parentUrlDomain.Any())
                    {
                        //  Add our parents
                        foreach (var urlDomain in parentUrlDomain)
                        {
                            foreach (var baseUrl in urlDomain.Urls)
                            {
                                if ((addTrailingSlash && baseUrl.EndsWith("/*/")) ||
                                    (!addTrailingSlash && baseUrl.EndsWith("/*")))
                                {
                                    continue;
                                }

                                var url = (baseUrl == "/") ? title : baseUrl + title;
                                if (!urls.Contains(url))
                                {
                                    urls.Add(url);
                                }
                            }
                        }
                    }
                    else if (!isHomePage && !urls.Contains(title))
                    {
                        urls.Add(title);
                    }

                    //  Add user defined urls
                    string[] userUrls;
                    if (userUrlsByLanguage.TryGetValue(language, out userUrls))
                    {
                        foreach (var add in userUrls)
                        {
                            if (!urls.Contains(add))
                            {
                                urls.Add(add);
                            }
                        }
                    }

                    //  Now add any wildcard paramaters
                    int urlParamterLevel = 0;
                    for (int level = 0; level != Schema.DocTypes.Constants.UrlParameters.Length; level++)
                    {
                        if (content.HasProperty(Schema.DocTypes.Constants.UrlParameters[level]))
                        {
                            urlParamterLevel = level + 1;
                        }
                    }
                    if (urlParamterLevel != 0)
                    {
                        var newUrls = new HashSet<string>();
                        StringBuilder parameter = new StringBuilder();
                        var segment = (addTrailingSlash) ? Schema.DocTypes.Constants.Wildcard + "/" : "/" + Schema.DocTypes.Constants.Wildcard;
                        for (int level = 0; level != urlParamterLevel; level++)
                        {
                            parameter.Append(segment);
                            foreach (var url in urls)
                            {
                                newUrls.Add(url + parameter.ToString());
                            }
                        }
                        urls.UnionWith(newUrls);
                    }

                    //  Add our urls to cache
                    Schema.Urls.CacheProvider.Current.Add(content.Id, new Schema.Urls.Multilingual() { Domain = domain.Item1, Language = language, Urls = urls.ToArray() });
                }
            }
        }

        internal void AddAll(UmbracoContext umbracoContext)
        {
            var addTrailingSlash = UmbracoConfig.For.UmbracoSettings().RequestHandler.AddTrailingSlash;
            var contentCache = umbracoContext.ContentCache; 
            var publishedSites = contentCache.GetByXPath("//" + Schema.DocTypes.Site.ModelTypeAlias);

            foreach (var publishedSite in publishedSites)
            {
                var site = Schema.ContentTypes.Site.Instance(publishedSite);
                var publishedPages = contentCache.GetByXPath("//* [@isDoc and starts-with(@path, '" + publishedSite.Path + ",')]");

                foreach (var publishedPage in publishedPages.OrderBy(x => x.Path))
                {
                    Add(site, new AnyContent(publishedPage), addTrailingSlash);
                }
            }
        }

        internal Schema.ContentTypes.Site Site(IContent content)
        {
            string path = (String.IsNullOrWhiteSpace(content.Path)) ? content.Parent().Path : content.Path;
            var siteId = int.Parse(path.Split(',')[1]);
            return Schema.ContentTypes.Site.Instance(siteId);
        }

        internal class DBLanguage
        {
            public int id { get; set; }
            public string languageISOCode { get; set; }
        }

        internal class DBDomain
        {
            public int id { get; set; }
            public int? domainDefaultLanguage { get; set; }
            public int domainRootStructureID { get; set; }
            public string domainName { get; set; }
        }

        internal IEnumerable<IDomain> Domains(ApplicationContext application)
        {
            //  Go and load new entries from database
            var languages = application.DatabaseContext.Database.Query<DBLanguage>
                ("SELECT [id],[languageISOCode] FROM [umbracoLanguage]").ToList();

            var domains = application.DatabaseContext.Database.Query<DBDomain>
                ("SELECT [id],[domainDefaultLanguage],[domainRootStructureID],[domainName] FROM [umbracoDomains]").ToList();

            var results = new List<Umbraco.Core.Models.UmbracoDomain>();
            foreach (var domain in domains)
            {
                string languageISOCode = "";
                if (domain.domainDefaultLanguage != null)
                {
                    var languageMatches = languages.Where(x => x.id == ((int) domain.domainDefaultLanguage));
                    if (languageMatches.Any())
                    {
                        languageISOCode = languageMatches.First().languageISOCode;
                    }
                }

                var result = new Umbraco.Core.Models.UmbracoDomain(domain.domainName, languageISOCode);
                result.Id = domain.id;
                result.LanguageId = domain.domainDefaultLanguage;
                result.RootContentId = domain.domainRootStructureID;
                results.Add(result);
            }

            return results;
        }
    }
}
