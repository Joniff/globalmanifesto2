﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace GlobalManifesto.Website.Management.Multilingual
{
    public class UmbracoRequest : Schema.UmbracoRequest.IUmbracoRequest
    {
        private UmbracoContext umbracoContext;
        private IDomain domain;
        public IDomain Domain
        {
            get
            {
                return domain;
            }
        }

        private IEnumerable<IDomain> domains;
        public IEnumerable<IDomain> Domains
        {
            get
            {
                if (domains != null)
                {
                    return domains;
                }
                return domains = Schema.Domains.DomainProvider.Current;
            }
        }

        private ILanguage language;
        public ILanguage Language
        {
            get
            {
                if (language == null || String.Compare(language.IsoCode, System.Threading.Thread.CurrentThread.CurrentUICulture.Name, true) != -1)
                {
                    return language = umbracoContext.Application.Services.LocalizationService.GetLanguageByIsoCode(System.Threading.Thread.CurrentThread.CurrentUICulture.Name);
                }
                return language;
            }
        }

        private ILanguage languageBackup;
        public ILanguage LanguageBackup
        {
            get
            {
                if (languageBackup != null)
                {
                    return languageBackup;
                }
                return languageBackup = Language;
            }
        }

        private IPublishedContent root;
        public IPublishedContent Root
        {
            get
            {
                return root;
            }
        }

        private Schema.ContentTypes.Site site;
        public Schema.ContentTypes.Site Site
        {
            get
            {
                return site;
            }
        }

        private string[] urlParamaters;
        public string[] UrlParamaters
        {
            get
            {
                return urlParamaters;
            }
            set
            {
                urlParamaters = value;
            }
        }

        internal UmbracoRequest(PublishedContentRequest contentRequest)
        {
            umbracoContext = contentRequest.RoutingContext.UmbracoContext;
            if (contentRequest.HasDomain)
            {
                //  By Domain
                domain = contentRequest.UmbracoDomain;
                var rootId = domain.RootContentId;
                if (rootId != null && rootId > 0)
                {
                    root = UmbracoContext.Current.ContentCache.GetById((int) rootId);
                }
            }
            if (root == null)
            {
                //  By Url
                var url = contentRequest.Uri.GetAbsolutePathDecoded();
                if (String.IsNullOrWhiteSpace(url))
                {
                    url = "/";
                }
                else if (url != "/")
                {
                    url = FirstSlug(url);
                }

                root = contentRequest.RoutingContext.UmbracoContext.ContentCache.GetByRoute(url);
                if (root == null)
                {
                    //  Get First site
                    root = UmbracoContext.Current.ContentCache.GetSingleByXPath("//" + Schema.DocTypes.Site.ModelTypeAlias);
                    if (root == null || root.Id < 1 || root.ContentType.Alias != Schema.DocTypes.Site.ModelTypeAlias)
                    {
                        return;
                    }
                }
                domain = Domains.Where(x => x.RootContentId == root.Id).First();
            }

            site = Schema.ContentTypes.Site.Instance(Culture());
        }

        internal UmbracoRequest(HttpContext context)
        {
#pragma warning disable 612,618
            umbracoContext = UmbracoContext.EnsureContext(new HttpContextWrapper(context), ApplicationContext.Current);
#pragma warning restore 612,618
            domain = GetDomain(context.Request.Url, Domains);
            if (domain != null && domain.RootContentId != null)
            {
                root = umbracoContext.ContentCache.GetById((int) domain.RootContentId);
                if (root.ContentType.Alias != Schema.DocTypes.Site.ModelTypeAlias)
                {
                    root = null;
                }
            }
            if (root == null)
            {
                //  By Url
                var url = context.Request.Url.GetAbsolutePathDecoded();
                if (String.IsNullOrWhiteSpace(url))
                {
                    url = "/";
                }
                else if (url != "/")
                {
                    url = FirstSlug(url);
                }

                root = umbracoContext.RoutingContext.UmbracoContext.ContentCache.GetByRoute(url);
                if (root == null)
                {
                    root = umbracoContext.ContentCache.GetSingleByXPath("//" + Schema.DocTypes.Site.ModelTypeAlias);
                }

                if (root == null || root.Id < 1 || root.ContentType.Alias != Schema.DocTypes.Site.ModelTypeAlias)
                {
                    return;
                }

                if (domain == null)
                {
                    domain = Domains.Where(x => x.RootContentId == root.Id).First();
                }
            }

            site = Schema.ContentTypes.Site.Instance(Culture());
        }

        private string FirstSlug(string url)
        {
            while (url.EndsWith("/"))
            {
                url = url.Substring(0, url.Length - 1);
                if (url.Length == 0)
                {
                    return null;
                }
            }
            var lastSlashPosition = url.LastIndexOf('/');
			if (lastSlashPosition > 0)
            {
				url = url.Substring(0, lastSlashPosition);
            }
            return url;
        }

        private IEnumerable<string> ContentNegotiation(string[] accepts)
        {
            var order = new SortedList<decimal, string>(Comparer<decimal>.Create((x, y) => y.CompareTo(x)));

            foreach (var accept in accepts)
            {
                if (String.IsNullOrWhiteSpace(accept))
                {
                    continue;
                }

                var values = accept.Split(new char[] {';', '\t' });
                if (String.IsNullOrWhiteSpace(values[0]))
                {
                    continue;
                }

                decimal relativeQualityFactor = 1.0M;
                if (values.Length != 1)
                {
                    var equations = values[1].Split(new char[] {'='});
                    if (equations.Length != 1)
                    {
                        decimal.TryParse(equations[1].Trim(), out relativeQualityFactor);
                    }
                }

                order.Add(relativeQualityFactor, values[0].Trim());
            }

            return order.Values;
        }

        private bool Match(ILanguage language, string matchingString)
        {
            return (String.Compare(language.CultureName, matchingString, false) == 0 ||
                String.Compare(language.CultureInfo.Name, matchingString, false) == 0 ||
                String.Compare(language.CultureInfo.DisplayName, matchingString, false) == 0 ||
                String.Compare(language.CultureInfo.EnglishName, matchingString, false) == 0 ||
                String.Compare(language.CultureInfo.TwoLetterISOLanguageName, matchingString, false) == 0 ||
                String.Compare(language.CultureInfo.ThreeLetterISOLanguageName, matchingString, false) == 0 ||
                String.Compare(language.CultureInfo.ThreeLetterWindowsLanguageName, matchingString, false) == 0) ? true : false;
        }

        private Schema.DocTypes.Site Culture()
        {
            var doctypeSite = new Schema.DocTypes.Site(root);
            languageBackup = UmbracoContext.Current.Application.Services.LocalizationService.GetLanguageById((int) domain.LanguageId);
            if (doctypeSite.Domains.PickedKeys.Contains(domain.DomainName))
            {
                language = languageBackup;
                System.Threading.Thread.CurrentThread.CurrentUICulture = language.CultureInfo;
            }
            else
            {
                var possibleLanguages = ContentNegotiation(System.Web.HttpContext.Current.Request.UserLanguages);
                var assignedLanguages = Schema.Domains.DomainProvider.Current.Where(x => x.RootContentId == root.Id && x.LanguageId != null).
                    Select(x => umbracoContext.Application.Services.LocalizationService.GetLanguageById((int) x.LanguageId));
                foreach (var possibleLanguage in possibleLanguages)
                {
                    var matches = assignedLanguages.Where(x => Match(x, possibleLanguage));
                    if (matches.Any())
                    {
                        language = matches.First();
                        System.Threading.Thread.CurrentThread.CurrentUICulture = language.CultureInfo;
                        break;
                    }
                }
            }

            return doctypeSite;
        }

        private IDomain GetDomain(Uri request, IEnumerable<IDomain> domains)
        {
            foreach (var domain in domains)
            {
                var builder = new UriBuilder(domain.DomainName);
                if (String.IsNullOrWhiteSpace(builder.Scheme))
                {
                    builder.Scheme = request.Scheme;
                }
                else if (String.Compare(builder.Scheme, request.Scheme, true) != 0)
                {
                    //  Schema doesn't match
                    continue;
                }
                if (builder.Port == 0)
                {
                    builder.Port = request.Port;
                }
                else if (builder.Port != request.Port)
                {
                    continue;
                }
                var url = builder.Uri;

                if (domain.DomainName.IndexOf('*') != -1)
                {
                    //  See if wildcards in host match
                    var callSegments = request.Host.Split('.');
                    var matchSegments = url.Host.Split('.');
                    if (callSegments.Length != matchSegments.Length)
                    {
                        continue;
                    }
                    var match = true;
                    for (var index = 0; index != callSegments.Length && match; index++)
                    {
                        if (String.Compare(matchSegments[index], "*", true) != 0 || String.Compare(matchSegments[index], callSegments[index], true) != 0)
                        {
                            match = false;
                        }
                    }
                    if (!match)
                    {
                        continue;
                    }
                }
                else if (String.Compare(request.Host, url.Host) != 0)
                {
                    //  Hosts don't match
                    continue;
                }
                if (!String.IsNullOrWhiteSpace(url.AbsolutePath) && String.Compare(url.AbsolutePath, 0, request.AbsolutePath, 0, url.AbsolutePath.Length, true) != 0)
                {
                    //  Domain has a path in it, and it doesn't match request
                    continue;
                }
                return domain;
            }

            return null;
        }
    }
}
