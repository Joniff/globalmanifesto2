﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using Umbraco.Core.Cache;
using Umbraco.Core.Models;

namespace GlobalManifesto.Website.Management.Multilingual
{
    public class Cache : Schema.Urls.ICache
    {
        private IRuntimeCacheProvider cacheProvider;
        public Cache(IRuntimeCacheProvider provider)
        {
            cacheProvider = provider;
        }

        private static string KeyByUrl(IDomain domain = null, ILanguage language = null, string url = null)
        {
            var key = new StringBuilder();
            key.Append("88498faa-6d9a-4f79-b50c-78d8efacc473:");
            if (domain == null)
            {
                return key.ToString();
            }
            key.Append(domain.Id);
            key.Append(':');

            if (language == null)
            {
                return key.ToString();
            }
            key.Append(language.IsoCode);
            key.Append(':');

            if (url == null)
            {
                return key.ToString();
            }

            key.Append(url);

            return key.ToString();
        }

        private static string KeyById(int? id = null, IDomain domain = null, ILanguage language = null)
        {
            var key = new StringBuilder();
            key.Append("2ff1da89-443e-4900-b80a-08d1ca0a31d2:");
            if (id == null)
            {
                return key.ToString();
            }
            key.Append((int) id);
            key.Append(':');

            if (domain == null)
            {
                return key.ToString();
            }
            key.Append(domain.Id);
            key.Append(':');

            if (language == null)
            {
                return key.ToString();
            }
            key.Append(language.IsoCode);
            key.Append(':');

            return key.ToString();
        }

        public void Remove()
        {
            cacheProvider.ClearCacheByKeySearch(KeyByUrl());
            cacheProvider.ClearCacheByKeySearch(KeyById());
        }

        public void Remove(int id)
        {
            var urlDomains = Get(id);
            if (urlDomains != null)
            {
                foreach (var urlDomain in urlDomains)
                {
                    foreach (var url in urlDomain.Urls)
                    {
                        cacheProvider.ClearCacheByKeySearch(KeyByUrl(urlDomain.Domain, urlDomain.Language, url));
                    }
                }

                cacheProvider.ClearCacheByKeySearch(KeyById(id));
            }
        }

        public void Add(int id, Schema.Urls.Multilingual urlDomain)
        {
            cacheProvider.InsertCacheItem(KeyById(id, urlDomain.Domain, urlDomain.Language), () => { return urlDomain;});
            foreach (var url in urlDomain.Urls)
            {
                cacheProvider.InsertCacheItem(KeyByUrl(urlDomain.Domain, urlDomain.Language, url), () => { return id; });
            }
        }

        public Schema.Urls.Multilingual Get(int? id, IDomain domain, ILanguage language)
        {
            return cacheProvider.GetCacheItem(KeyById(id, domain, language)) as Schema.Urls.Multilingual;
        }

        public IEnumerable<Schema.Urls.Multilingual> Get(int id)
        {
            return cacheProvider.GetCacheItemsByKeySearch<Schema.Urls.Multilingual>(KeyById(id));
        }

        public int? Get(IDomain domain, ILanguage language, string url)
        {
            return cacheProvider.GetCacheItem(KeyByUrl(domain, language, url)) as int?;
        }

        public bool Match(IEnumerable<Schema.Urls.Multilingual> sources, IEnumerable<Schema.Urls.Multilingual> compares)
        {
            if (sources.Count() != compares.Count())
            {
                return false;
            }
            foreach (var source in sources)
            {
                bool match = false;
                foreach (var compare in compares)
                {
                    if (source.Domain.Id == compare.Domain.Id &&
                        source.Language.Id == compare.Language.Id &&
                        source.Urls.SequenceEqual(compare.Urls))
                    {
                        match = true;
                        break;
                    }
                }
                if (!match)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
