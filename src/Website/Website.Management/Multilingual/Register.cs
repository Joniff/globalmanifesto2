﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core;
using Umbraco.Core.Cache;
using Umbraco.Core.Configuration;
using Umbraco.Core.Models;
using Umbraco.Core.Services;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace GlobalManifesto.Website.Management.Multilingual
{
    public class Register : ApplicationEventHandler
    {
        protected override void ApplicationStarting(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Core.Logging.LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "#globalmanifesto-register2: ApplicationStarting");

            ContentFinderResolver.Current.InsertTypeBefore<ContentFinderByNiceUrl, MultilingualContentFinder>();
            ContentFinderResolver.Current.RemoveType<ContentFinderByNiceUrl>();

            UrlProviderResolver.Current.InsertTypeBefore<DefaultUrlProvider, MultilingualUrlProvider>();
            UrlProviderResolver.Current.RemoveType<DefaultUrlProvider>();
        }

        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Core.Logging.LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "#globalmanifesto-register5: ApplicationStarted");

            DomainService.Saved += DomainService_Saved;
            DomainService.Deleted += DomainService_Deleted;

            Umbraco.Core.Publishing.PublishingStrategy.Publishing += PublishingStrategy_Publishing;
            ContentService.Published += ContentService_Published;
            ContentService.UnPublished += ContentService_UnPublished;

            Schema.Urls.CacheProvider.Current = new Cache(applicationContext.ApplicationCache.RuntimeCache);
            var multilingual = new Multilingual();
            Schema.Domains.DomainProvider.Current = multilingual.Domains(applicationContext);
            multilingual.AddAll(umbracoApplication.Context.GetUmbracoContext());
        }

        private void DomainService_Deleted(IDomainService sender, Umbraco.Core.Events.DeleteEventArgs<IDomain> e)
        {
            Schema.Domains.DomainProvider.Current = new Multilingual().Domains(UmbracoContext.Current.Application);
        }

        private void DomainService_Saved(IDomainService sender, Umbraco.Core.Events.SaveEventArgs<IDomain> e)
        {
            Schema.Domains.DomainProvider.Current = new Multilingual().Domains(UmbracoContext.Current.Application);
        }

        private string FormatUrl(string url, bool addTrailingSlash)
        {
            url = String.Join("/", url.Split('/').ToList().Select(x => x.ToUrlSegment()));
            if (!url.StartsWith("/"))
            {
                url = "/" + url;
            }
            if (addTrailingSlash)
            {
                if (!url.EndsWith("/"))
                {
                    url = url + "/";
                }
            }
            else
            {
                while (url.Length > 1 && url.EndsWith("/") )
                {
                    url = url.Substring(0, url.Length - 1);
                }
            }
            return url;
        }

        private void PublishingStrategy_Publishing(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            if (!e.IsAllRepublished)
            {
                var contents = e.PublishedEntities.Where(x => x.HasProperty(Schema.DocTypes.Constants.UmbracoUrls));
                if (!contents.Any())
                {
                    return;
                }

                var addTrailingSlash = UmbracoConfig.For.UmbracoSettings().RequestHandler.AddTrailingSlash;
                foreach (var content in contents)
                {
                    //  Make sure the Urls are in the right format - TODO: need to create a new property type that can do this
                    var urlsByLanguage = new Schema.DataTypes.Vorto<string[]>(content.GetValue<string>(Schema.DocTypes.Constants.UmbracoUrls));
                    var oldProperty = urlsByLanguage.Property;
                    IDictionary<ILanguage, string[]> newUrlsByLanguage = new Dictionary<ILanguage, string[]>();
                    foreach (var urls in urlsByLanguage.Values)
                    {
                        var newUrls = new List<string>();
                        foreach (var url in urls.Value)
                        {
                            if (!String.IsNullOrEmpty(url))
                            {
                                newUrls.Add(FormatUrl(url, addTrailingSlash));
                            }
                        }
                        newUrlsByLanguage.Add(urls.Key, newUrls.ToArray());
                    }
                    urlsByLanguage.Values = newUrlsByLanguage;
                    var newProperty = urlsByLanguage.Property;
                    if (String.Compare(oldProperty, newProperty) != 0)
                    { 
                        content.SetValue(Schema.DocTypes.Constants.UmbracoUrls, urlsByLanguage.Property);
                    }
                }
            }
        }

        private void ContentService_Published(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            var multilingual =  new Multilingual();
            if (e.IsAllRepublished)
            {
                multilingual.RemoveAll();
                multilingual.AddAll(UmbracoContext.Current);
            }
            else
            {
                var cascade = new List<string>();

                //  Any content is site node with updated home picker
                var home = e.PublishedEntities.Where(x => x.ContentType.Alias == Schema.DocTypes.Site.ModelTypeAlias && x.IsPropertyDirty(Schema.DocTypes.Constants.Home));
                if (home.Any())
                {
                    //  Update all children
                    cascade.AddRange(home.Select(x => x.Path));
                }

                //  Any content have UmbracoUrls
                var contents = e.PublishedEntities.Where(x => x.HasProperty(Schema.DocTypes.Constants.UmbracoUrls));
                var addTrailingSlash = UmbracoConfig.For.UmbracoSettings().RequestHandler.AddTrailingSlash;
                if (contents.Any())
                {
                    foreach (var content in contents.OrderBy(x => x.Path != "").ThenBy(x => x.Path))     //  Process all entries except new content
                    {
                        //  Update cache
                        var originalUrlDomains = Schema.Urls.CacheProvider.Current.Get(content.Id);
                        multilingual.Remove(content.Id);
                        multilingual.Add(multilingual.Site(content), new AnyContent(content), addTrailingSlash);

                        //  See if the urls have changed, if so will need to update children
                        if (!String.IsNullOrWhiteSpace(content.Path) && !Schema.Urls.CacheProvider.Current.Match(originalUrlDomains, Schema.Urls.CacheProvider.Current.Get(content.Id)))
                        {
                            if (!cascade.Where(x => String.Compare(x, 0, content.Path, 0, x.Length) == 0).Any())
                            {
                                cascade.Add(content.Path);
                            }
                        }
                    }
                }

                //  Update any children that need it
                if (cascade.Any())
                {
                    var processed = new HashSet<int>();
                    foreach (var path in cascade)
                    {
                        var siteId = int.Parse(path.Split(',')[1]);
                        var site = Schema.ContentTypes.Site.Instance(siteId);
                        var publishedPages = UmbracoContext.Current.ContentCache.GetByXPath("//* [@isDoc and starts-with(@path, '" + path + ",')]");

                        foreach (var publishedPage in publishedPages.OrderBy(x => x.Path))
                        {
                            if (!processed.Contains(publishedPage.Id))
                            {
                                multilingual.Remove(publishedPage.Id);
                                multilingual.Add(site, new AnyContent(publishedPage), addTrailingSlash);
                                processed.Add(publishedPage.Id);
                            }
                        }
                    }
                }
            }
        }

        private void ContentService_UnPublished(Umbraco.Core.Publishing.IPublishingStrategy sender, Umbraco.Core.Events.PublishEventArgs<IContent> e)
        {
            var multilingual =  new Multilingual();

            foreach (var content in e.PublishedEntities)
            {
                multilingual.Remove(content.Id);
            }
        }
    }
}
