﻿using Archetype.Models;
using Newtonsoft.Json;
using Our.Umbraco.Vorto.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace GlobalManifesto.Website.Management.Multilingual
{
    //  This is a super class that can hold a IPublishedContent or IContent
    internal class AnyContent
    {
        private object content = null;

        public AnyContent(object value)
        {
            content = value;
        }

        public string Alias
        {
            get
            {
                if (content is IPublishedContent)
                {
                    return ((IPublishedContent) content).DocumentTypeAlias;
                }
                if (content is IContent)
                {
                    return ((IContent) content).ContentType.Alias;
                }
                throw new NotSupportedException();
            }
        }

        public T GetPropertyValue<T>(string name)
        {
            if (content is IPublishedContent)
            {
                if (typeof(T) == typeof(VortoValue))
                {
                    return (T) (object) JsonConvert.DeserializeObject<VortoValue>((string) ((IPublishedContent) content).GetProperty(name).DataValue);
                }
                return ((IPublishedContent) content).GetPropertyValue<T>(name);
            }
            if (content is IContent)
            {
                if (typeof(T) == typeof(VortoValue))
                {
                    return (T) (object) JsonConvert.DeserializeObject<VortoValue>(((IContent) content).GetValue<string>(name));
                }
                return ((IContent) content).GetValue<T>(name);
            }
            throw new NotSupportedException();
        }

        public bool HasProperty(string name)
        {
            if (content is IPublishedContent)
            {
                return ((IPublishedContent) content).HasProperty(name);
            }
            if (content is IContent)
            {
                return ((IContent) content).HasProperty(name);
            }
            throw new NotSupportedException();
        }

        public string Path
        {
            get
            {
                if (content is IPublishedContent)
                {
                    return ((IPublishedContent) content).Path;
                }
                if (content is IContent)
                {
                    return ((IContent) content).Path;
                }
                throw new NotSupportedException();
            }
        }

        public string Name
        {
            get
            {
                if (content is IPublishedContent)
                {
                    return ((IPublishedContent) content).Name;
                }
                if (content is IContent)
                {
                    return ((IContent) content).Name;
                }
                throw new NotSupportedException();
            }
        }

        public IDictionary<ILanguage, string> Titles
        {
            get
            {
                if (HasProperty(Schema.DocTypes.Constants.Title))
                {
                    return new Schema.DataTypes.Vorto<string>(GetPropertyValue<VortoValue>(Schema.DocTypes.Constants.Title)).Values;
                }
                var pageTitles = new Dictionary<ILanguage, string>();
                var seoEditor = new Schema.DataTypes.Vorto<ArchetypeModel>(GetPropertyValue<VortoValue>(Schema.DocTypes.Constants.Seo));
                if (seoEditor != null)
                {
                    foreach (var item in seoEditor.Values)
                    {
                        if (item.Value.Fieldsets.Any())
                        {
                            pageTitles.Add(item.Key, item.Value.Fieldsets.First().GetValue<string>(Schema.DocTypes.Constants.SeoTitle));
                        }
                    }
                }
                return pageTitles;
            }
        }

        public IDictionary<ILanguage, string[]> Urls
        {
            get
            {
                if (!HasProperty(Schema.DocTypes.Constants.UmbracoUrls))
                {
                    return null;
                }
                return new Schema.DataTypes.Vorto<string[]>(GetPropertyValue<VortoValue>(Schema.DocTypes.Constants.UmbracoUrls)).Values;
            }
        }

        public int Id
        {
            get
            {
                if (content is IPublishedContent)
                {
                    return ((IPublishedContent) content).Id;
                }
                if (content is IContent)
                {
                    return ((IContent) content).Id;
                }
                throw new NotSupportedException();
            }
        }

        public int ParentId
        {
            get
            {
                if (content is IPublishedContent)
                {
                    if (!String.IsNullOrWhiteSpace(((IPublishedContent) content).Path))
                    {
                        var paths = ((IPublishedContent) content).Path.Split(',');
                        int parentId = -1;
                        if (paths.Length != 0 && int.TryParse(paths[paths.Length - 2], out parentId))
                        {
                            return parentId;
                        }
                    }
                    if (((IPublishedContent) content).Parent != null)
                    {
                        return ((IPublishedContent) content).Parent.Id;
                    }
                    return Umbraco.Core.Constants.System.Root;
                }
                if (content is IContent)
                {
                    return ((IContent) content).ParentId;
                }
                throw new NotSupportedException();
            }
        }
    }
}
