﻿using System;
using System.Collections.Generic;
using System.Linq;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Routing;

namespace GlobalManifesto.Website.Management.Multilingual
{
    public class MultilingualUrlProvider : IUrlProvider
    {
        private bool AuthorityMatch(Uri source, Uri match)
        {
            return (String.Compare(source.Scheme, match.Scheme, true) == 0 &&
                source.Port == match.Port &&
                String.Compare(source.Host, match.Host, true) == 0) ? true : false;
        }

        private IEnumerable<string> Urls(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode, IDomain domainFilter = null, ILanguage languageFilter = null)
        {
            var unordered = new List<Tuple<IDomain, ILanguage, Uri>>();
            //var cache = kernel.Get<Schema.Urls.ICacheProvider>();
            var urlDomains = Schema.Urls.CacheProvider.Current.Get(id);
            foreach (var urlDomain in urlDomains.Where(x => (domainFilter == null || x.Domain.Id == domainFilter.Id) && (languageFilter == null || x.Language.Id == languageFilter.Id)))
            {
                foreach (var rawUrl in urlDomain.Urls)
                {
                    var url = new UriBuilder(urlDomain.Domain.DomainName + rawUrl);
                    if (String.IsNullOrWhiteSpace(url.Scheme))
                    {
                        url.Host = current.Scheme;
                    }
                    if (url.Port == 0)
                    {
                        url.Port = current.Port;
                    }
                    unordered.Add(new Tuple<IDomain, ILanguage, Uri>(urlDomain.Domain, urlDomain.Language, url.Uri));
                }
            }

            //  Order urls with local host at top
            var results = new List<string>();
            foreach (var ordered in unordered.OrderBy(x => !AuthorityMatch(current, x.Item3)).ThenBy(x => x.Item1.Id).ThenBy(x => x.Item2.IsoCode))
            {
                var url = (mode != UrlProviderMode.Absolute && AuthorityMatch(current, ordered.Item3)) ? ordered.Item3.PathAndQuery : ordered.Item3.AbsoluteUri;
                if (!results.Where(x => String.Compare(x, url, true) == 0).Any())
                {
                    results.Add(mode != UrlProviderMode.Absolute && (AuthorityMatch(current, ordered.Item3)) ? ordered.Item3.PathAndQuery : ordered.Item3.AbsoluteUri);
                }
            }
            return results;
        }

        public string GetUrl(UmbracoContext umbracoContext, int id, Uri current, UrlProviderMode mode)
        {
            var umbracoRequest = Schema.UmbracoRequest.UmbracoRequestProvider.Current;
            var urls = Urls(umbracoContext, id, current, mode, (umbracoRequest != null) ? umbracoRequest.Domain : null, (umbracoRequest != null) ? umbracoRequest.Language : null);
            if (!urls.Any())
            {
                return "";     //  This node can't be accessed via an URL (Sites, Global data, etc. Can't be accessed from the Frontend)
            }
            return urls.First();
        }

        public IEnumerable<string> GetOtherUrls(UmbracoContext umbracoContext, int id, Uri current)
        {
            var umbracoRequest = Schema.UmbracoRequest.UmbracoRequestProvider.Current;
            return Urls(umbracoContext, id, current, UrlProviderMode.Auto, (umbracoRequest != null) ? umbracoRequest.Domain : null, (umbracoRequest != null) ? umbracoRequest.Language : null).Skip(1);
        }
    }
}
