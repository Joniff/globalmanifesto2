﻿using System;
using Umbraco.Core;
using Umbraco.Core.Services;


namespace GlobalManifesto.Website.Management.InitializeContent
{
    public class Sitemap : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Core.Logging.LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "#globalmanifesto-register7: ApplicationStarted");

            ContentService.Created += ContentService_Creating;
        }

        private void ContentService_Creating(IContentService sender, Umbraco.Core.Events.NewEventArgs<Umbraco.Core.Models.IContent> e)
        {
            //  Defaultly Allow this page to be indexed 
            if (e.Entity.HasProperty("sitemapIndex"))
            {
                e.Entity.SetValue("sitemapIndex", true);
            }
        }
    }
}
