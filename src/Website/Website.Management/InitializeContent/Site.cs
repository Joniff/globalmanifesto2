﻿using System;
using Umbraco.Core;
using Umbraco.Core.Services;

namespace GlobalManifesto.Website.Management.InitializeContent
{
    public class Site : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Core.Logging.LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "#globalmanifesto-register6: ApplicationStarted");

            ContentService.Created += ContentService_Creating;
        }

        private void ContentService_Creating(IContentService sender, Umbraco.Core.Events.NewEventArgs<Umbraco.Core.Models.IContent> e)
        {
            //  Defaultly Allow this page to be indexed 
            if (e.Entity.ContentType.Alias == Schema.DocTypes.Site.ModelTypeAlias && e.Entity.HasProperty("robotstxt"))
            {
                e.Entity.SetValue("robotstxt", "User-agent: *\nDisallow: /umbraco/\nsitemap: " + RobotsTxt.RobotsTxtHandler.DomainVariable + "/sitemap.xml");
            }
        }
    }
}
