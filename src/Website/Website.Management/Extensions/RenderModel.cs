﻿using System;
using Umbraco.Web.Models;

namespace GlobalManifesto.Website.Management.Extensions
{
    public static class ExtensionRenderModel
    {
        public static Schema.UmbracoRequest.IUmbracoRequest UmbracoRequest(this RenderModel model)
        {
            return Schema.UmbracoRequest.UmbracoRequestProvider.Current;
        }
    }
}
