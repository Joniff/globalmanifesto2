﻿using System;

namespace GlobalManifesto.Website.Management.Extensions
{
    public static class ExtensionIHtmlString
    {
        public static bool Any(this System.Web.IHtmlString value)
        {
            return (value != null && value.ToString().Length != 0) ? true: false;
        }
    }

}
