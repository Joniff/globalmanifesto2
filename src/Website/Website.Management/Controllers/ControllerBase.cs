﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace GlobalManifesto.Website.Management.Controllers
{
    public abstract class ControllerBase : Umbraco.Web.Mvc.RenderMvcController
    {
        public Schema.UmbracoRequest.IUmbracoRequest UmbracoRequest
        {
            get
            {
                return Schema.UmbracoRequest.UmbracoRequestProvider.Current;
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //  Have to reset language again
            System.Threading.Thread.CurrentThread.CurrentUICulture = UmbracoRequest.Language.CultureInfo;
        }

        public override ActionResult Index(RenderModel model)
        {
            //Do some stuff here, then return the base method
            return base.Index(model);
        }

        /// <summary>
        /// Gets fired whenever there is an unhandled exception with a controller
        /// </summary>
        /// <param name="filterContext"></param>
        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }
            filterContext.ExceptionHandled = true;

            if (UmbracoRequest.Site == null || UmbracoRequest.Site.ServerError == null)
            {
                return;
            }
            string url = UmbracoRequest.Site.ServerError.Url;
            if (String.Compare(url, filterContext.HttpContext.Request.Url.PathAndQuery) == 0)
            {
                //  We are already trying to show the error page - and have errored
                return;
            }

            filterContext.HttpContext.Response.StatusDescription =  filterContext.Exception.Message;
            HttpContext.Server.TransferRequest(url, false);
        }

        protected ActionResult RedirectToErrorPage()
        {
            if (UmbracoRequest.Site == null || UmbracoRequest.Site.ServerError == null)
            {
                return null;
            }
            string url = UmbracoRequest.Site.Error.Url;
            if (String.Compare(url, HttpContext.Request.Url.PathAndQuery) == 0)
            {
                //  We are already trying to show the error page - and have errored
                return null;
            }
            HttpContext.Server.TransferRequest(url, false);
            return Redirect(url);
        }

        protected ActionResult RedirectToServerErrorPage()
        {
            if (UmbracoRequest.Site == null || UmbracoRequest.Site.ServerError == null)
            {
                return null;
            }
            string url = UmbracoRequest.Site.ServerError.Url;
            if (String.Compare(url, HttpContext.Request.Url.PathAndQuery) == 0)
            {
                //  We are already trying to show the error page - and have errored
                return null;
            }
            HttpContext.Server.TransferRequest(url, false);
            return Redirect(url);
        }
    }
}
