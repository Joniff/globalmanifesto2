﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using Umbraco.Core;

namespace GlobalManifesto.Website.Management.RobotsTxt
{
    public class Register : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Core.Logging.LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "#globalmanifesto-register3: ApplicationStarted");

            RouteTable.Routes.Add(new Route
                (
                    "robots.txt",
                    new RobotsTxtHandler()
                ));
        }
    }
}
