﻿using System;
using System.Web;
using System.Web.Routing;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace GlobalManifesto.Website.Management.RobotsTxt
{

    public class RobotsTxtHandler : IHttpHandler, IRouteHandler
    {
        internal const string DomainVariable = "{{DOMAIN}}";

        public void ProcessRequest(HttpContext context)
        {
            Schema.UmbracoRequest.UmbracoRequestProvider.Current = new Multilingual.UmbracoRequest(context);
            if (Schema.UmbracoRequest.UmbracoRequestProvider.Current.Site == null || 
                Schema.UmbracoRequest.UmbracoRequestProvider.Current.Site.Error == null)
            {
                context.Response.Clear();
                context.Response.StatusCode = (int) System.Net.HttpStatusCode.InternalServerError;
                context.Response.ContentType = "text/plain";
                context.Response.End();
                return;
            }

            var contents = Schema.UmbracoRequest.UmbracoRequestProvider.Current.Site.RobotsTxt.Replace(
                DomainVariable, context.Request.Url.GetLeftPart(UriPartial.Authority));

            context.Response.Clear();
            context.Response.ContentType = "text/plain";
            context.Response.Write(contents);
            context.Response.End();
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }
    }
}
