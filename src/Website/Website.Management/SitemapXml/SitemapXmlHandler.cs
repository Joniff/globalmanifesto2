﻿using System;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Web;
using System.Web.Routing;
using System.Xml.Linq;
using Umbraco.Core;
using Umbraco.Core.Models;
using Umbraco.Web;

namespace GlobalManifesto.Website.Management.SitemapXml
{

    public class SitemapXmlHandler : IHttpHandler, IRouteHandler
    {
        private XDocument SitemapXml(Schema.ContentTypes.Site site)
        {
            XNamespace xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";
            XElement root = new XElement(xmlns + "urlset");
 
            foreach (var node in site.SitemapXml)
            {
                XElement urlElement = new XElement(
                    xmlns + "url",
                    new XElement(xmlns + "loc", node.Url),
                    node.LastModified == null ? null : new XElement(
                        xmlns + "lastmod", 
                        node.LastModified.Value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:sszzz")),
                    node.Frequency == null ? null : new XElement(
                        xmlns + "changefreq", 
                        node.Frequency.ToLowerInvariant()),
                    node.Priority == null ? null : new XElement(
                        xmlns + "priority", 
                        node.Priority.Value.ToString("F1", CultureInfo.InvariantCulture)));
                root.Add(urlElement);
            }
  
           return new XDocument(root);;            
        }

        public void ProcessRequest(HttpContext context)
        {
            Schema.UmbracoRequest.UmbracoRequestProvider.Current = new Multilingual.UmbracoRequest(context);
            if (Schema.UmbracoRequest.UmbracoRequestProvider.Current.Site == null)
            {
                context.Response.Clear();
                context.Response.StatusCode = (int) System.Net.HttpStatusCode.InternalServerError;
                context.Response.ContentType = "text/plain";
                context.Response.End();
                return;
            }

            context.Response.Clear();
            context.Response.ContentType = "application/xml";
            SitemapXml(Schema.UmbracoRequest.UmbracoRequestProvider.Current.Site).Save(context.Response.OutputStream);
            context.Response.End();
        }

        public bool IsReusable
        {
            get { return true; }
        }

        public IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            return this;
        }
    }
}
