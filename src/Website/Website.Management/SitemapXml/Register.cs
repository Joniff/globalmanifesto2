﻿using System;
using System.Web.Routing;
using Umbraco.Core;

namespace GlobalManifesto.Website.Management.SitemapXml
{
    public class Register : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            Umbraco.Core.Logging.LogHelper.Info(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType, "#globalmanifesto-register4: ApplicationStarted");

            RouteTable.Routes.Add(new Route
                (
                    "sitemap.xml",
                    new SitemapXmlHandler()
                ));
        }
    }
}
