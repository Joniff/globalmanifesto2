﻿using nuPickers.Shared.DotNetDataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GlobalManifesto.Website.Management.NuPickerDataSource
{
    public class Cultures : IDotNetDataSource
    {
        IEnumerable<KeyValuePair<string, string>> IDotNetDataSource.GetEditorDataItems(int contextId)
        {
            var cultures = new SortedList<string, KeyValuePair<string,string>>();
            var languages = Umbraco.Web.UmbracoContext.Current.Application.Services.LocalizationService.GetAllLanguages();
            foreach (var language in languages)
            {
                cultures.Add(language.CultureInfo.EnglishName, new KeyValuePair<string, string>(language.CultureName, language.CultureInfo.EnglishName));
            }

            return cultures.Values;
        }
    }
}
