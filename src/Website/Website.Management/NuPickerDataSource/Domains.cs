﻿using nuPickers.Shared.DotNetDataSource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Umbraco.Web;

namespace GlobalManifesto.Website.Management.NuPickerDataSource
{
    public class Domains : IDotNetDataSource
    {
        IEnumerable<KeyValuePair<string, string>> IDotNetDataSource.GetEditorDataItems(int contextId)
        {
            var results = new List<KeyValuePair<string,string>>();
            foreach (var domain in Schema.Domains.DomainProvider.Current.Where(x => x.RootContentId == contextId))
            {
                results.Add(new KeyValuePair<string, string>(domain.DomainName, domain.DomainName + " = " + domain.LanguageIsoCode));
            }
            return results;
        }
    }
}
